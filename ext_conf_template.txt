# cat=general/a; type=int; label= Administrator backend user uid: All of this users widgets will be shared with everyone
dashboardAdminBackendUserUid = 0
# cat=general/b; type=boolean; label= Auto-Create Default dashboards: If user has no custom dashboard, create default dashboards for user
createDefaultDashboards = 0
# cat=general/c; type=int; label= Height(px) for sys news list widgets : If 0 standard large height will apply
sysNewsListWidgetHeight = 0
# cat=general/d; type=string; label= Matomo token auth: token for accessing Matomo API
matomoAuthToken = bb0cb8015d0e7626a5a6c4f878f38ea0
# cat=general/e; type=string; label= Matomo URL: address for accessing Matomo API
matomoURL =