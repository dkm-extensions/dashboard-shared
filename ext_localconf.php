<?php
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Dashboard\DashboardInitializationService::class] = [
    'className' => \DKM\DashboardShared\DashboardInitializationService::class
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Dashboard\DashboardRepository::class] = [
    'className' => \DKM\DashboardShared\DashboardRepository::class
];