<?php
$EM_CONF[$_EXTKEY] = array(
    'title' => 'Dashboard - shared',
    'description' => 'Add mandatory shared dashboards',
    'category' => 'misc',
    'version' => '1.2.0',
    'state' => 'experimental',
    'author' => 'Stig Nørgaard Færch',
    'author_email' => 'snf@dkm.dk',
    'author_company' => 'Danish Church Mediacenter',
    'constraints' => array(
        'depends' => array(),
        'conflicts' => array(),
        'suggests' => array(),
    ),
);
