<?php

namespace DKM\DashboardShared\Configuration;

use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\Exception\MissingArrayPathException;
use TYPO3\CMS\Core\Utility\GeneralUtility;

abstract class AbstractFeature implements FeatureInterface
{
    private ?Site $site = null;

    private string $featureName;

    public function initialize(string $featureName): self
    {
        if(method_exists($this, 'findSite')) {
            $this->setSite($this->findSite());
        }
        $this->setFeatureName($featureName);
        return $this;
    }

    public function setSite(?Site $site): void
    {
        $this->site = $site;
    }

    public function getSite(): ?Site
    {
        return $this->site;
    }

    public function getActionType($subFeatureName = null): string
    {
        if($this->isEnabled($subFeatureName)) {
            return self::ACTION_TYPE_DISABLE;
        } else {
            if (!$this->hasBeenInitialized($subFeatureName)) {
                return self::ACTION_TYPE_CREATE_AND_ENABLE;
            }
            return self::ACTION_TYPE_ENABLE;
        }
    }

    public function getState($subFeatureName = null): string
    {
        if($this->isEnabled($subFeatureName)) {
            return self::STATE_ENABLED;
        } else {
            if (!$this->hasBeenInitialized($subFeatureName)) {
                return self::STATE_INITIAL;
            }
            return self::STATE_DISABLED;
        }
    }

    private function getPath($subFeatureName = null): string
    {
        return "settings.features.{$this->featureName}" . ($subFeatureName ? ".{$subFeatureName}" : '');
    }

    public function isEnabled($subFeatureName = null): bool
    {
        try {
            return ArrayUtility::getValueByPath($this->site->getConfiguration(), $this->getPath($subFeatureName) . '.enable', '.') ?? false;
        } catch (MissingArrayPathException $e) {
            return false;
        }
    }

    public function enable($subFeatureName = null): bool
    {
//        $this->getSite()->
        return $this->setFeatureEnabledState(true, $subFeatureName);
    }

    public function disable($subFeatureName = null): bool
    {
        return $this->setFeatureEnabledState(false, $subFeatureName);
    }

    private function setFeatureEnabledState(bool $enable, $subFeature = null): bool
    {
        $featurePath = $this->featureName . ($subFeature ? ".{$subFeature}" : '');
        GeneralUtility::makeInstance(\DKM\SiteConfiguration\SiteConfiguration::class)->setFeature($this->site->getIdentifier(), $featurePath, $enable);
        return true;
    }

    public function hasBeenInitialized($subFeatureName = null): bool
    {

        try {
            ArrayUtility::getValueByPath($this->site->getConfiguration(), $this->getPath($subFeatureName) . '.enable', '.');
            return true;
        } catch (MissingArrayPathException $e) {
            return false;
        }
    }

    public function setFeatureName(string $featureName): void
    {
        $this->featureName = $featureName;
    }

//    /**
//     * @param array $configurationToMerge
//     * @return void
//     * @throws \TYPO3\CMS\Core\Configuration\Exception\SiteConfigurationWriteException
//     */
//    public function updateSiteConfiguration(array $configurationToMerge)
//    {
//
//
////        $configuration = $this->site->getConfiguration();
////        ArrayUtility::mergeRecursiveWithOverrule($configuration, $configurationToMerge);
////        /** @var SiteConfiguration $siteConfiguration */
////        $siteConfiguration = GeneralUtility::makeInstance(SiteConfiguration::class);
////        /** @var SiteMatcher $siteMatcher */
////        $siteMatcher = GeneralUtility::makeInstance(SiteMatcher::class);
////        $siteConfigurationData = Yaml::parse(file_get_contents(Environment::getConfigPath() . "/sites/{$this->site->getIdentifier()}/config.yaml"));
////        $siteConfiguration->write($this->site->getIdentifier(), $configuration);
//    }

}