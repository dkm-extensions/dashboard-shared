<?php

namespace DKM\DashboardShared\Configuration;

use TYPO3\CMS\Core\Site\Entity\Site;

interface FeatureInterface
{
    /**
     * ACTION TYPES
     */
    const ACTION_TYPE_CREATE_AND_ENABLE = 'create';
    const ACTION_TYPE_ENABLE = 'enable';
    const ACTION_TYPE_DISABLE = 'disable';
    public function getActionType(): string;

    /**
     * STATES
     */
    const STATE_INITIAL = 'initial';
    const STATE_ENABLED = 'enabled';
    const STATE_DISABLED = 'disabled';
    public function getState(): string;


    public function initialize(string $featureName): self;

    public function getSite(): ?Site;
    public function setSite(?Site $site): void;

    public function setFeatureName(string $featureName);

    public function isEnabled($subFeatureName = null): bool;

    public function enable($subFeatureName = null): bool;

    public function disable($subFeatureName = null): bool;

    public function hasBeenInitialized($subFeatureName = null): bool;

    public function activate($subFeatureName = null): bool;

    public function destroy(): bool;

    public function getConfiguration(): array;
}