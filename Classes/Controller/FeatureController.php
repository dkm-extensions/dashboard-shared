<?php

declare(strict_types=1);

/*
 * This file is part of the "dashboard_shared" extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace DKM\DashboardShared\Controller;

use DKM\DashboardShared\Configuration\FeatureInterface;
use DKM\DashboardShared\Widgets\FeatureWidget;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Command\CacheFlushCommand;
use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @internal
 */
final class FeatureController
{
    public function __construct(
private readonly ResponseFactoryInterface $responseFactory
    ) {
    }

    /**
     * Toggle and initialize feature
     *
     * @throws \JsonException
     */
    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        if($widgetIdentifier = $request->getParsedBody()['widgetIdentifier'] ?? false) {
            /** @var FeatureWidget $featureWidget */
            $featureWidget = GeneralUtility::getContainer()->get("dashboard.widget.{$widgetIdentifier}");

            /** @var FeatureInterface $featureService */
            $featureService = $featureWidget->getInstanceFeatureServiceClass();
            $featureService->initialize($widgetIdentifier);

            $buttonValue = $request->getParsedBody()['buttonValue'] ?? null;

            switch ($featureService->getActionType($buttonValue)) {
                case FeatureInterface::ACTION_TYPE_CREATE_AND_ENABLE:
                    $success = $featureService->activate($buttonValue);
                    break;
                case FeatureInterface::ACTION_TYPE_ENABLE:
                    $success = $featureService->enable($buttonValue);
                    break;
                case FeatureInterface::ACTION_TYPE_DISABLE:
                    $success = $featureService->disable($buttonValue);
                    break;
            }
            if($success ?? false) {
                $options = $featureWidget->getOptions();
                if (($options['cacheFlushRootPageIdTagPrefix'] ?? false) && ($options['flushCacheOnAllActions'] ?? false)) {
                    $cacheTag = "{$options['cacheFlushRootPageIdTagPrefix']}{$featureService->getSite()->getRootPageId()}";
                    GeneralUtility::makeInstance(CacheManager::class)->getCache('pages')->flushByTag($cacheTag);
                }
            }
        }

//        $parameters = $request->getParsedBody();
//        if (! \is_array($parameters)) {
//            return $this->buildResponse(true, 'Given parameters in body cannot be converted to an array');
//        }
//        try {
//            $this->checkParameters(
//                (string)($parameters['siteIdentifier'] ?? ''),
//                (string)($parameters['date'] ?? ''),
//                (string)($parameters['note'] ?? '')
//            );
//        } catch (\InvalidArgumentException $e) {
//            return $this->buildResponse(true, $e->getMessage());
//        }
//
//        if (! $this->hasUserPermissionForWidget()) {
//            return $this->buildResponse(true, $this->translate('widgets.createAnnotation.error.noPermission'));
//        }
//
//        try {
//            $this->createAnnotation();
//        } catch (\Throwable $t) {
//            $this->logger->error($t->getMessage());
//
//            return $this->buildResponse(true, 'An error occurred, please have a look into the TYPO3 log file for details.');
//        }
//
        return $this->buildResponse();
    }


    private function buildResponse(bool $isError = false, string $message = ''): ResponseInterface
    {
        $data = [
            'status' => $isError ? 'error' : 'success',
        ];
        if ($message !== '') {
            $data['message'] = $message;
        }

        $response = $this->responseFactory->createResponse()
            ->withHeader('Content-Type', 'application/json; charset=utf-8');
        $response->getBody()->write(\json_encode($data, \JSON_THROW_ON_ERROR));

        return $response;
    }
}
