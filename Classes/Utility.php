<?php

namespace DKM\DashboardShared;

use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

class Utility
{

    protected const MODULE_DATA_LAST_DASHBOARD_VIEW_TIMESTAMP = 'dashboard/last_view_timestamp/';
    protected const MODULE_DATA_TODAY_DASHBOARD_VIEW_TIMESTAMP = 'dashboard/today_view_timestamp/';

    public static function isDashboardAdmin(): bool
    {
        $dashboardAdminBackendUserUid = self::getExtensionConfigurationValue('dashboardAdminBackendUserUid', 0);
        return $GLOBALS['BE_USER']->user['uid'] == $dashboardAdminBackendUserUid;
    }

    /**
     * @param $path
     * @param $defaultValue
     * @return mixed|null
     */
    public static function getExtensionConfigurationValue($path, $defaultValue = null) {
        try {
            return GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('dashboard_shared', $path);
        } catch (ExtensionConfigurationExtensionNotConfiguredException|ExtensionConfigurationPathDoesNotExistException $e) {
            return $defaultValue;
        }
    }

    /**
     * @param $identifier
     */
    public static function isDashboardShared($identifier): bool
    {
        return strpos($identifier, '_') === 0;
    }

    /**
     * @throws ExtensionConfigurationExtensionNotConfiguredException
     * @throws ExtensionConfigurationPathDoesNotExistException
     */
    public static function getIdSiteFromHostName(): int
    {
        static $idSite = null;
        if ($idSite !== null) {
            return $idSite;
        }
        if($credentials = self::getAPICredentials())
        {
            $url = $credentials['matomoURL'] . "?module=API&method=SitesManager.getSitesIdFromSiteUrl&url={$GLOBALS['TYPO3_REQUEST']->getUri()->getHost()}&format=JSON&token_auth={$credentials['matomoAuthToken']}";
        } else  {
            return 0;
        }

        return json_decode(GeneralUtility::getUrl($url), true)[0]['idsite'] ?? 0;
    }

    /**
     * @throws ExtensionConfigurationExtensionNotConfiguredException
     * @throws ExtensionConfigurationPathDoesNotExistException
     * @throws Exception
     */
    public static function getAPICredentials(): array
    {
        $dashboardSharedConfig = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('dashboard_shared');
        if(ExtensionManagementUtility::isLoaded('matomo_widgets') &&
            ($dashboardSharedConfig['matomoAuthToken'] ?? false) &&
            ($dashboardSharedConfig['matomoURL'] ?? false))
        {
            return ['matomoAuthToken' => $dashboardSharedConfig['matomoAuthToken'], 'matomoURL' => rtrim($dashboardSharedConfig['matomoURL'], '/')];
        } else {
            throw new Exception('Required matomoAuthToken and/or matomoURL extension configuration not set!');
        }
    }

    /**
     * Gets news from sys_news and converts them into a format suitable for
     * showing them at the login screen.
     *
     * @param $connectionName
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public static function getSystemNews($connectionName = 'Default'): array
    {
        if (!($GLOBALS['TYPO3_CONF_VARS']['DB']['Connections'][$connectionName] ?? false)) {
            return [];
        }

        Utility::setDashboardTimestamps($connectionName);
        $systemNewsTable = 'sys_news';
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionByName($connectionName)->createQueryBuilder();
        $systemNews = [];
        $systemNewsRecords = $queryBuilder
            ->select('title', 'content', 'crdate')
            ->from($systemNewsTable)->orderBy('crdate', 'DESC')->executeQuery()->fetchAllAssociative();
        foreach ($systemNewsRecords as $systemNewsRecord) {
            $systemNews[ $systemNewsRecord['crdate'] > Utility::getDashboardLastViewDateTime($connectionName)->getTimestamp() ? 'new' : 'old' ][$systemNewsRecord['crdate']] = [
                'crdate' => $systemNewsRecord['crdate'],
                'header' => $systemNewsRecord['title'],
                'content' => $systemNewsRecord['content'],
                'connectionName' => $connectionName
            ];
        }
        return $systemNews;
    }

    public static function getBackendUser(): BackendUserAuthentication
    {
        return $GLOBALS['BE_USER'];
    }

    public static function getDashboardLastViewDateTime(string $key = ''): \DateTime
    {
        return self::getDashboardDateTime(self::MODULE_DATA_LAST_DASHBOARD_VIEW_TIMESTAMP . $key);
    }

    /**
     * @return void
     */
    public static function setDashboardTimestamps($key = '') {
        $storedTodayDateTime = self::getDashboardTodayViewDateTime($key);
        $storedTodayDateTime->setTime(0,0);
        $timeToday = new \DateTime('today');
        self::setDashboardTodayViewTimestamp($key, $timeToday->getTimestamp());
        if($timeToday > $storedTodayDateTime) {
            self::getBackendUser()->pushModuleData(self::MODULE_DATA_LAST_DASHBOARD_VIEW_TIMESTAMP . $key, $timeToday->getTimestamp());
        }
    }

    public static function getDashboardTodayViewDateTime(string $key = ''): \DateTime
    {
        return self::getDashboardDateTime(self::MODULE_DATA_TODAY_DASHBOARD_VIEW_TIMESTAMP . $key);
    }

    /**
     * @param $connectionName
     * @return void
     */
    public static function setDashboardTodayViewTimestamp($key = '', $time = null) {
        if ($time === null) {
            $time = time();
        }
        self::getBackendUser()->pushModuleData(self::MODULE_DATA_TODAY_DASHBOARD_VIEW_TIMESTAMP . $key, $time);
    }

    public static function getDashboardDateTime($module): \DateTime
    {
        $timestamp = (int)self::getBackendUser()->getModuleData($module);
        return (new \DateTime($timestamp !== 0 ? "@{$timestamp}" : '@0'))->setTimezone(new \DateTimeZone(date_default_timezone_get()));
    }

}