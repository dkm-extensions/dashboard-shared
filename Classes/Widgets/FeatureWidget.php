<?php

declare(strict_types=1);

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

namespace DKM\DashboardShared\Widgets;

use DKM\DashboardShared\Configuration\FeatureInterface;
use DKM\DashboardShared\Widgets\Provider\FeatureDataProvider;
use DKM\TreS\Configuration\NewsletterFeature;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Backend\View\BackendViewFactory;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Page\JavaScriptModuleInstruction;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Dashboard\Widgets\AdditionalJavaScriptInterface;
use TYPO3\CMS\Dashboard\Widgets\ButtonProviderInterface;
use TYPO3\CMS\Dashboard\Widgets\JavaScriptInterface;
use TYPO3\CMS\Dashboard\Widgets\RequestAwareWidgetInterface;
use TYPO3\CMS\Dashboard\Widgets\WidgetConfigurationInterface;
use TYPO3\CMS\Dashboard\Widgets\WidgetInterface;
use TYPO3\CMS\Fluid\View\StandaloneView;

/**
 * Concrete CTA button implementation
 *
 * Shows a widget with a CTA button to easily go to a specific page or do a specific action. You can add a button to the
 * widget by defining a button provider.
 *
 * The following options are available during registration:
 * - text           string          Adds a text to the widget to give some more background information about
 *                                  what a user can expect when clicking the button. You can either enter a
 *                                  normal string or a translation string (eg. LLL:EXT:dashboard/Resources/Private/Language/locallang.xlf:widgets.documentation.gettingStarted.text)
 * @see ButtonProviderInterface
 */
class FeatureWidget implements WidgetInterface, RequestAwareWidgetInterface, JavaScriptInterface
{
    private ServerRequestInterface $request;

    public function __construct(
        private readonly WidgetConfigurationInterface $configuration,
        private readonly        FeatureDataProvider          $dataProvider,
        private readonly        string                       $featureServiceClass,
        private readonly        string                       $LLLOverridePath,
        private readonly        BackendViewFactory          $backendViewFactory,
        private readonly        array                        $options = [],
    )
    {

        $path = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($this->LLLOverridePath);

        $files = array_filter(GeneralUtility::getFilesInDir($path),function ($v) {
            return strpos($v, 'FeatureWidget.xlf');
        });
        foreach ($files as $file) {
            if(str_contains($file, 'FeatureWidget.xlf')) {
                $firstPart = explode('.', $file)[0];
                if($firstPart !== 'FeatureWidget') {
                    $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride'][$firstPart]['EXT:dashboard_shared/Resources/Private/Language/FeatureWidget.xlf'][] = $this->LLLOverridePath . $file;
                } else {
                    $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:dashboard_shared/Resources/Private/Language/FeatureWidget.xlf'][] = $this->LLLOverridePath . $file;
                }
            }
        }
    }

    public function setRequest(ServerRequestInterface $request): void
    {
        $this->request = $request;
    }


    public function renderWidgetContent(): string
    {
        $view = $this->backendViewFactory->create($this->request);
        if(class_exists($this->featureServiceClass)) {

            $featureService = $this->getInstanceFeatureServiceClass();
            $featureService->initialize();

            $this->dataProvider->getFeatureData();
        // Method to get site root pid
            if($site = $featureService->getSite()) {
                $subfeatures = $this->options['subfeatures'] ?? [];
                foreach ($subfeatures as &$subfeature) {
                    $subfeature['actionType'] = $featureService->getActionType($subfeature['identifier']);
                    $subfeature['state'] = $featureService->getState($subfeature['identifier']);
                }
                $view->assignMultiple([
                    'text' => $this->options['text'] ?? '',
                    'actionType' => $featureService->getActionType(),
                    'state' => $featureService->getState(),
                ]);
            }
            $view->assignMultiple([
                'site' => $site,
                'options' => array_merge($this->options, ['subfeatures' => $subfeatures ?? null]),
                'configuration' => $this->configuration,
            ]);
        }
        return $view->render('Widget/FeatureWidget');
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @return FeatureInterface
     */
    public function getInstanceFeatureServiceClass(): FeatureInterface
    {
        return GeneralUtility::makeInstance($this->featureServiceClass);
    }

//    public function getJsFiles(): array
//    {
//        return [
//            JavaScriptModuleInstruction::create('@dkm/dashboard-shared/FeatureWidget.js')
//        ];
//    }


    public function getJavaScriptModuleInstructions(): array
    {
        return [
            JavaScriptModuleInstruction::create('@dkm/dashboard-shared/feature-widget.js')->invoke('initialize')
        ];
    }
}
