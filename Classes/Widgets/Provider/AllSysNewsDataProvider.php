<?php

namespace DKM\DashboardShared\Widgets\Provider;

use DKM\DashboardShared\Utility;
use TYPO3\CMS\Dashboard\Widgets\ListDataProviderInterface;

class AllSysNewsDataProvider implements ListDataProviderInterface
{
    public function getItems(): array
    {
        $news = Utility::getSystemNews();
        $sharedNews = Utility::getSystemNews('shared_sys_news');
        $new = array_replace($news['new'] ?? [], $sharedNews['new'] ?? []);
        ksort($new);
        $old = array_replace($news['old'] ?? [], $sharedNews['old'] ?? []);
        ksort($old);
        return ['new' => array_reverse($new), 'old' => array_reverse($old)];
    }

}