<?php

namespace DKM\DashboardShared\Widgets\Provider;

use DKM\DashboardShared\Utility;
use TYPO3\CMS\Dashboard\Widgets\ListDataProviderInterface;

class SharedSysNewsDataProvider implements ListDataProviderInterface
{
    public function getItems(): array
    {
        return Utility::getSystemNews('shared_sys_news');
    }
}