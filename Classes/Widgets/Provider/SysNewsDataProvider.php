<?php

namespace DKM\DashboardShared\Widgets\Provider;

use DKM\DashboardShared\Utility;
use TYPO3\CMS\Dashboard\Widgets\ListDataProviderInterface;

class SysNewsDataProvider implements ListDataProviderInterface
{
    public function getItems(): array
    {
        return Utility::getSystemNews();
    }
}