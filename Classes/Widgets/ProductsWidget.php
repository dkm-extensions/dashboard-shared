<?php

declare(strict_types=1);

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

namespace DKM\DashboardShared\Widgets;

use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Backend\View\BackendViewFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Dashboard\Widgets\RequestAwareWidgetInterface;
use TYPO3\CMS\Fluid\View\StandaloneView;
use TYPO3\CMS\Dashboard\Widgets\WidgetInterface;
use TYPO3\CMS\Dashboard\Widgets\WidgetConfigurationInterface;
use TYPO3\CMS\Dashboard\Widgets\Provider\FeatureDataProvider;
use TYPO3\CMS\Dashboard\Widgets\AdditionalCssInterface;

class ProductsWidget implements WidgetInterface, RequestAwareWidgetInterface, AdditionalCssInterface
{


    /**
     * @var array
     */
    private $options;
    private ServerRequestInterface $request;
    private \TYPO3\CMS\Core\View\ViewInterface $view;

    public function __construct(
        private readonly    WidgetConfigurationInterface $configuration,
        private readonly    BackendViewFactory          $backendViewFactory,

        array $options = []
    ) {
        $this->options = [
            'limit' => 5,
        ] + $options;
    }

    public function renderWidgetContent(): string
    {
        $this->view = $this->backendViewFactory->create($this->request);
        $this->view->assignMultiple([
            'options' => $this->options,
            'configuration' => $this->configuration,
        ]);
        return $this->view->render('Widget/ProductsWidget');
    }

    public function getCssFiles(): array
    {
        return [
            'EXT:dashboard_shared/Resources/Public/CSS/Widgets/products.css'
        ];
    }

    public function getOptions(): array
    {
        return [];
    }

    public function setRequest(ServerRequestInterface $request): void
    {
        $this->request = $request;
    }
}