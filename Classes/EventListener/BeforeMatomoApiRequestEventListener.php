<?php

namespace DKM\DashboardShared\EventListener;

use Brotkrueml\MatomoWidgets\Event\BeforeMatomoApiRequestEvent;
use DKM\DashboardShared\Utility;

class BeforeMatomoApiRequestEventListener
{
    public function __invoke(BeforeMatomoApiRequestEvent $event): void
    {
        if(strpos($GLOBALS['TYPO3_REQUEST']->getQueryParams()['widget'], 'matomo_widgets.__sharedDashboard') === 0 && ($idSiteFromHostName = Utility::getIdSiteFromHostName())) {
            $event->setIdSite($idSiteFromHostName);
        }
    }
}