import DocumentService from "@typo3/core/document-service.js";
import RegularEvent from "@typo3/core/event/regular-event.js";
import AjaxRequest from "@typo3/core/ajax/ajax-request.js";
import Notification from "@typo3/backend/notification.js"

class FeatureWidget {
    initialize() {
        const FORM_SELECTOR = 'form[data-dashboardshared-featuretoggleandinitialize]';
        const ROUTE_KEY = 'dashboard_shared_feature_toggle_and_initialize';

        const sendRequest = (event) => {
            const formElement = event.target;
            const widgetIdentifierElement = formElement.querySelector('input[name=widget_identifier]');
            const submitElement = event.submitter;
            const notificationTitle = submitElement.dataset.notificationTitle;
            const parameters = {
                widgetIdentifier: widgetIdentifierElement.value,
                buttonValue: submitElement.value
            };
            new AjaxRequest(TYPO3.settings.ajaxUrls[ROUTE_KEY]).post(parameters).then(
                async response => {
                    const data = await response.resolve();
                    if (!data.status) {
                        Notification.error(notificationTitle, formElement.dataset.notificationUnknownError);
                    } else if (data.status === 'success') {
                        Notification.success(notificationTitle, submitElement.dataset.notificationSuccess);
                        // noteElement.value = '';
                        top.TYPO3.ModuleMenu.App.refreshMenu();
                        location.reload();
                    } else {
                        Notification.error(notificationTitle, data.message);
                    }
                    // submitElement.disabled = false;
                }, error => {
                    Notification.error(notificationTitle, formElement.dataset.notificationHttpError);
                    // submitElement.disabled = false;
                }
            );
        }
        DocumentService.ready().then(() => {
            new RegularEvent('submit', function (event) {
                event.preventDefault();
                event.target.querySelector('button[type=submit]').disabled = true;
                sendRequest(event);
            }).delegateTo(document, FORM_SELECTOR);
        });
    }
}

export default new FeatureWidget;
