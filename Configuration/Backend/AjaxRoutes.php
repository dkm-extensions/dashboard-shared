<?php

declare(strict_types=1);

/*
 * This file is part of the "matomo_widgets" extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

return [
    'dashboard_shared_feature_toggle_and_initialize' => [
        'path' => '/dashboard_shared/feature-toggle-and-initialize',
        'methods' => ['POST'],
        'target' => \DKM\DashboardShared\Controller\FeatureController::class,
    ],
    'dashboard_shared_disable_feature' => [
        'path' => '/dashboard_shared/feature-disable',
        'methods' => ['GET'],
        'target' => \DKM\DashboardShared\Controller\FeatureController::class,
    ],
];
