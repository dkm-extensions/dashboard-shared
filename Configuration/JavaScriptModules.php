<?php
return [
    'imports' => [
        '@dkm/dashboard-shared/' => 'EXT:dashboard_shared/Resources/Public/JavaScript/',
        '@dkm/dashboard-shared/feature-widget.js' => 'EXT:dashboard_shared/Resources/Public/JavaScript/feature-widget.js',
    ],
];